import { MongoClient, ObjectId } from "mongodb"
import CONFIG from './index'

const URL = CONFIG.protocol === "https" ? `${CONFIG.protocol}://${CONFIG.host}` : `${CONFIG.protocol}://${CONFIG.host}:${CONFIG.port}`

const getMongoClient = () => {
  return new MongoClient(CONFIG.mongoDbUrl, { useUnifiedTopology: true })
}

const getDeviantDbAndClient = async () => {
  const client = getMongoClient()

  let db = null

  try {
    await client.connect()

    db = client.db(CONFIG.dbName)
  } catch (err) {
    console.log(err.stack);
  }

  return {
    client,
    db
  }
}

const getUserWithId = async (userId) => {
  const { client, db } = await getDeviantDbAndClient()

  let user = null
  try {
    const col = db.collection('users')

    const docs = await (col.find({ _id: ObjectId(userId) }).limit(1)).toArray()

    user = docs[0]
  } catch (err) {
    console.log('---- MongoUtils.getUserWithId() ----');
    console.log(err.stack);
  }

  client.close()

  if (user) {
    user.id = user._id
    delete user._id
  }

  return user
}

const getUserWithEmail = async (userEmail) => {
  const { client, db } = await getDeviantDbAndClient()

  let user = null
  try {
    const col = db.collection('users')

    const docs = await (col.find({ email: userEmail }).limit(1)).toArray()

    user = docs[0]
  } catch (err) {
    console.log('---- MongoUtils.getUserWithEmail() ----');
    console.log(err.stack);
  }

  client.close()

  if (user) {
    user.id = user._id
    delete user._id
  }

  return user
}

const insertUser = async (user) => {
  const { client, db } = await getDeviantDbAndClient()

  let insertedUser = null
  try {
    const col = db.collection('users')

    const insert = await col.insertOne(user)
    if (insert.result.ok === 1) {
      insertedUser = await getUserWithEmail(user.email)
    }
  } catch (err) {
    console.log('---- MongoUtils.insertUser() ----');
    console.log(err.stack);
  }

  client.close()

  return insertedUser
}

const updateUser = async (userId, user) => {
  const { client, db } = await getDeviantDbAndClient()

  let updatedUser = null
  try {
    const col = db.collection('users')

    const update = await col.updateOne({ _id: ObjectId(userId) }, { $set: user })
    if (update.result.ok === 1) {
      updatedUser = await getUserWithId(userId)
    }
  } catch (err) {
    console.log('---- MongoUtils.updateUser() ----');
    console.log(err.stack);
  }

  client.close()

  return updatedUser
}

const insertAvatar = async (avatar) => {
  const { client, db } = await getDeviantDbAndClient()

  let insertedAvatar = null
  delete avatar._id
  try {
    const col = db.collection('avatars')

    const insert = await col.insertOne(avatar)
    if (insert.result.ok === 1) {
      insertedAvatar = await getAvatarWithParams({ index: avatar.index })
    }
  } catch (err) {
    console.log(err.stack);
  }

  client.close()

  return insertedAvatar
}

const insertAvatars = async (avatars) => {
  const { client, db } = await getDeviantDbAndClient()

  let result = false
  try {
    const col = db.collection('avatars')

    const insert = await col.insertMany(avatars.map(({ _id, ...avatar }) => avatar))
    if (insert.result.ok === 1) {
      result = true
    }
  } catch (err) {
    console.log(err.stack);
  }

  client.close()

  return result
}

const getAvatarWithParams = async (avatarParams) => {
  const { client, db } = await getDeviantDbAndClient()

  let avatar = null
  try {
    const col = db.collection('avatars')

    const docs = await (col.find(avatarParams).limit(1)).toArray()

    avatar = docs[0]
  } catch (err) {
    console.log(err.stack);
  }

  client.close()

  return avatar
}

const getAvatarsWithParams = async (avatarParams) => {
  const { client, db } = await getDeviantDbAndClient()

  let avatars = []
  try {
    const col = db.collection('avatars')

    avatars = await (col.find(avatarParams)).toArray()

  } catch (err) {
    console.log(err.stack);
  }

  client.close()

  return avatars
}

const getAvatarWithId = async (id) => {
  const { client, db } = await getDeviantDbAndClient()

  let avatar = null
  try {
    const col = db.collection('avatars')

    const docs = await (col.find({ _id: new ObjectId(id) })).toArray()

    avatar = docs[0]
  } catch (err) {
    console.log(err.stack);
  }

  client.close()

  return avatar
}

const deleteAvatar = async (id) => {
  const { client, db } = await getDeviantDbAndClient()
  let result = false

  try {
    const col = db.collection('avatars')
    const removeRequest = await col.deleteOne({ _id: new ObjectId(id) })
    if (removeRequest.result.ok === 1) {
      result = true
    }

  } catch (err) {
    console.log(err.stack);
  }

  client.close()

  return result
}

const updateAvatar = async (avatarId, avatar) => {
  const { client, db } = await getDeviantDbAndClient()

  let updatedAvatar = null
  delete avatar._id
  try {
    const col = db.collection('avatars')

    const update = await col.updateOne({ _id: new ObjectId(avatarId) }, { $set: avatar })
    if (update.result.ok === 1) {
      updatedAvatar = await getAvatarWithId(avatarId)
    }
  } catch (err) {
    console.log(err.stack);
  }

  client.close()

  return updatedAvatar
}

export default {
  getMongoClient,
  getDeviantDbAndClient,
  getUserWithId,
  getUserWithEmail,
  insertUser,
  updateUser,
  insertAvatar,
  insertAvatars,
  getAvatarWithParams,
  getAvatarsWithParams,
  getAvatarWithId,
  deleteAvatar,
  updateAvatar
}