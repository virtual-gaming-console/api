import { google } from 'googleapis';
import axios from 'axios'

const googleConfig = {
  clientId: '313613869378-3qpu8apimbgg9dsoh9rd53qlgevj4ers.apps.googleusercontent.com',
  clientSecret: 'CAKIxvtDeSn9phsCGvS_HivI',
  redirect: 'https://api.playdeviant.com/user/google-auth'
};

/**
 * Create the google auth object which gives us access to talk to google's apis.
 */
function createConnection() {
  return new google.auth.OAuth2(
    googleConfig.clientId,
    googleConfig.clientSecret,
    googleConfig.redirect
  );
}

/**
 * This scope tells google what information we want to request.
 */
const defaultScope = [
  'https://www.googleapis.com/auth/plus.me',
  'https://www.googleapis.com/auth/userinfo.email',
];

/**
 * Get a url which will open the google sign-in page and request access to the scope provided (such as calendar events).
 */
function getConnectionUrl(auth) {
  return auth.generateAuthUrl({
    access_type: 'offline',
    prompt: 'consent', // access type and approval prompt will force a new refresh token to be made each time signs in
    scope: defaultScope
  });
}

/**
 * Create the google url to be sent to the client.
 */
const urlGoogle = () => {
  const auth = createConnection(); // this is from previous step
  const url = getConnectionUrl(auth);
  return url;
}

/**
 * Helper function to get the library with access to the google plus api.
 */
function getGooglePlusApi(auth) {
  return google.plus({ version: 'v1', auth });
}

/**
 * Extract the email and id of the google account from the "code" parameter.
 */
const getGoogleAccountFromCode = async code => {

  const auth = createConnection();

  const data = await auth.getToken(code);
  const tokens = data.tokens;

  const response = await axios({
    url: 'https://www.googleapis.com/oauth2/v2/userinfo',
    method: 'get',
    headers: {
      Authorization: `Bearer ${tokens.access_token}`,
    },
  });

  return {
    id: response.data.id,
    email: response.data.email,
    tokens: tokens, // you can save these to the user if you ever want to get their details without making them log in again
  };
}

export default {
  urlGoogle,
  getGoogleAccountFromCode
}