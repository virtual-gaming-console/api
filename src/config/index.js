import DEV_CONFIG from './config.dev'
import PROD_CONFIG from './config.prod'

const CONFIG = process.env.NODE_ENV.trim() === "development" ? DEV_CONFIG : PROD_CONFIG

export default CONFIG