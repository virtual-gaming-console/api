import compression from 'compression'
import express, { static as _static } from 'express'
import helmet from 'helmet'
import cookieParser from 'cookie-parser'
import uniqid from 'uniqid'
import methodOverride from 'method-override'
import { urlencoded, json } from 'body-parser'
import cors from 'cors'
import CONFIG from './config'
import WebSocket from 'ws'

const app = express()
const http = require('http').createServer(app)
const wss = new WebSocket.Server({ server: http });
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');

app.disable('x-powered-by');
app.use(cors())
app.use(compression())
app.use(helmet())
app.use(methodOverride('X-HTTP-Method-Override'))
app.use(cookieParser())
app.use(urlencoded({ extended: false }))
app.use(json())
app.use('/assets', _static(__dirname + '/../public'));

import UserController from './controllers/UserController'
UserController.init()
app.use('*', UserController.authMiddleware)
app.use('/user', UserController.router())

import GameController from './controllers/GameController'
app.use('/game', GameController.router())

import LobbyController from './controllers/LobbyController'
app.use('/lobby', LobbyController.router())

import AvatarController from './controllers/AvatarController'
app.use('/avatar', AvatarController.router())

AvatarController.initDb(() => {
    wss.on('connection', function connection(socket) {
        socket.isAlive = true;
        socket.id = uniqid()

        socket.on('pong', () => {
            socket.isAlive = true
        })

        socket.rooms = []
        socket.customAttributes = []
        socket.broadcast = (criteria, message) => {
            wss.clients.forEach(function each(client) {
                if (client.readyState === WebSocket.OPEN && client.rooms[0] === socket.rooms[0] && criteria(client, socket)) {
                    client.send(message);
                }
            });
        }

        socket.broadcastToOthers = (criteria, message) => {
            socket.broadcast((client, socket) => client !== socket && criteria(client, socket), message)
        }

        socket.join = roomCode => {
            socket.rooms = [...socket.rooms.filter(x => x !== roomCode), roomCode]
        }

        socket.leave = roomCode => {
            socket.rooms = [...socket.rooms.filter(x => x !== roomCode)]
        }

        socket.attr = () => socket.customAttributes

        socket.getMemberSockets = () => {
            const wsTab = []

            wss.clients.forEach(ws => {
                if (ws.attr().memberId === socket.attr().memberId) {
                    wsTab.push(ws)
                }
            })

            return wsTab
        }

        socket.getMemberLastMessageTime = () => {
            const memberSockets = socket.getMemberSockets()
            let time = memberSockets.pop().attr().lastMessageTime

            memberSockets.forEach(ws => {
                if (ws.attr().lastMessageTime > time) {
                    time = ws.attr().lastMessageTime
                }
            })

            return time
        }

        socket.on('message', message => {
            const text = decoder.write(message)
            const json = JSON.parse(text)

            socket.attr().lastMessageTime = Date.now()

            switch (json.title) {
                case 'join-room':
                    socket.join(json.data.code)
                    socket.attr().isController = json.data.isController
                    if (json.data.memberId) {
                        socket.attr().memberId = json.data.memberId
                    }
                    if (json.data.attributes) {
                        socket.customAttributes = {
                            ...socket.customAttributes,
                            ...json.data.attributes
                        }
                    }
                    socket.send(JSON.stringify({ title: 'join-room-success', data: { room: json.data.code } }))
                    break
                default:
                    break
            }

            LobbyController.onMessage(socket, json)
            UserController.onMessage(socket, json)
        });
    });

    const interval = setInterval(function ping() {
        wss.clients.forEach(function each(ws) {
            if (ws.isAlive === false && (Date.now() - ws.getMemberLastMessageTime()) > 300000) {
                LobbyController.disconnectLobby({ code: ws.rooms[0], member: { id: ws.attr().memberId } }, ws)
                UserController.onDisconnect(ws)
                return ws.terminate();
            }

            ws.isAlive = false
            ws.ping()
        });
    }, 30000)

    http.listen(CONFIG.port, function () {
        console.log(`Api listening on port ${CONFIG.port}!`)
    })
})