import CONFIG from '../config'
import uniqid from 'uniqid'

const URL = CONFIG.protocol === "https" ? `${CONFIG.protocol}://${CONFIG.host}` : `${CONFIG.protocol}://${CONFIG.host}:${CONFIG.port}`

class LobbyMember {
  id = null
  nickname = null
  picture = null
  isLobbyLeader = false
  socket = null
  loggedIn = false

  constructor(id, nickname, picture, isLobbyLeader) {
    this.id = id || uniqid()
    if (id) {
      this.loggedIn = true
    }
    this.nickname = nickname || 'unknown user'
    var index = 1 + Math.round(Math.random() * 150)
    var isShiny = (1 + Math.round(Math.random() * 9)) === 10
    this.picture = picture || `${URL}/assets/images/avatars/${isShiny ? "x" : ""}${index}.png`
    this.isLobbyLeader = isLobbyLeader
  }

  get = () => {
    return {
      id: this.id,
      nickname: this.nickname,
      picture: this.picture,
      isLobbyLeader: this.isLobbyLeader,
      loggedIn: this.loggedIn,
    }
  }
}

export default LobbyMember