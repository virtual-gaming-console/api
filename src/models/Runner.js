import UserController from "../controllers/UserController"
import MongoUtils from '../config/mongo-utils'
import AvatarController from '../controllers/AvatarController'

class Runner {
  name = "runner"
  clock = null
  timeout = null
  state = null
  screenSocket = null
  startDate = null
  lastUpdateDate = null
  onStop = null
  players = null
  members = null

  config = {
    speed: 1,
    rows: 3,
    columns: 15,
    timeBeforeStarting: 1000,
    timeBetweenTicks: 500,
    timeToIncreaseSpeed: 10000,
  }

  get stopped() {
    return this.clock === null || this.clock === undefined
  }

  constructor(socket, config) {
    this.screenSocket = socket
    this.config = {
      ...this.config,
      ...config
    }
  }

  areEquals(first, second) {
    if (first.length !== second.length) {
      return false
    }
    for (let i = 0; i < first.length; i++) {
      if (!second.includes(first[i])) {
        return false
      }
    }
    return true;
  }

  initState() {
    const lastScoreBoard = [...((this.state && this.state.scoreboard) || [])].sort((a, b) => a.points - b.points)
    this.state = {
      speed: this.config.speed,
      spawnedObjects: 0,
      destroyedObjects: 0,
      scoreboard: [],
      world: []
    }
    for (let index = 0; index < this.config.rows; index++) {
      this.state.world.push(new Array(this.config.columns).fill('0'))
    }
    if (!this.players || !this.areEquals(this.players.map(playerItem => playerItem.playerId), this.members.map(member => '' + member.id))) {
      this.members.map(member => '' + member.id).forEach((playerId, index) => {
        this.state.world[1][index] = playerId
        this.state.scoreboard.push({ playerId, points: 0 })
      })
      this.players = this.members.map(member => '' + member.id).map(playerId => ({ playerId: playerId, timeout: null }))
    } else {
      lastScoreBoard.forEach((score, index) => {
        this.state.world[1][index] = score.playerId
        this.state.scoreboard.push({ playerId: score.playerId, points: 0 })
      })
      this.players = lastScoreBoard.map(score => ({ playerId: score.playerId, timeout: null }))
    }
  }

  start() {
    this.initState()
    this.sendState()

    this.startDate = Date.now()
    this.clock = setInterval(() => {
      if (this.someoneIsStillAlive()) {
        this.updateState()
      } else {
        this.restart()
      }
    }, 50)
    // if (this.onStop) {
    //   this.onStop()
    // }
  }

  restart() {
    this.stop()
    this.sendState()

    this.timeout = setTimeout(() => {
      this.start()
    }, 5000)
  }

  someoneIsStillAlive() {
    for (let rowIndex = 0; rowIndex < this.state.world.length; rowIndex++) {
      const row = this.state.world[rowIndex];
      if (row.find(tile => tile.length > 1)) {
        return true
      }
    }

    return false
  }

  processInput(playerId, input) {
    const player = this.players.find(player => player.playerId === playerId)
    if (!player) {
      return
    } else {
      const playerIndex = this.players.findIndex(playerItem => playerItem.playerId === player.playerId)
      if (!this.getPlayerColumn(playerIndex).find(tile => tile === player.playerId)) {
        return
      }

      switch (input) {
        case 'up':
          this.playerJump(player, playerIndex)
          break
        case 'down':
          this.playerCrouch(player, playerIndex)
          break
        default:
          break
      }
    }
  }

  getPlayerColumn(playerIndex) {
    return [
      this.state.world[0][playerIndex],
      this.state.world[1][playerIndex],
      this.state.world[2][playerIndex],
    ]
  }

  playerJump(player, playerIndex) {
    if (this.getPlayerColumn(playerIndex)[0] === player.playerId) {
      clearTimeout(player.timeout)
      player.timeout = setTimeout(() => this.resetToIdlePosition(player, playerIndex), 1000)
      return
    }


    if (['0', '3'].find(type => this.getPlayerColumn(playerIndex)[0] === type)) {
      if (this.getPlayerColumn(playerIndex)[0] === '3') {
        this.state.destroyedObjects++
        this.creditOneBoxToAllPlayers()
      }

      this.state.world[0][playerIndex] = player.playerId
      this.state.world[1][playerIndex] = '0'
      this.state.world[2][playerIndex] = '0'

      this.sendState()

      clearTimeout(player.timeout)
      player.timeout = setTimeout(() => this.resetToIdlePosition(player, playerIndex), 1000)
    } else {
      this.state.world[1][playerIndex] = '0'
      this.state.world[2][playerIndex] = '0'

      this.sendState()
    }
  }

  playerCrouch(player, playerIndex) {
    if (this.getPlayerColumn(playerIndex)[2] === player.playerId) {
      clearTimeout(player.timeout)
      player.timeout = setTimeout(() => this.resetToIdlePosition(player, playerIndex), 1000)
      return
    }


    if (['0', '3'].find(type => this.getPlayerColumn(playerIndex)[2] === type)) {
      if (this.getPlayerColumn(playerIndex)[2] === '3') {
        this.state.destroyedObjects++
        this.creditOneBoxToAllPlayers()
      }

      this.state.world[0][playerIndex] = '0'
      this.state.world[1][playerIndex] = '0'
      this.state.world[2][playerIndex] = player.playerId

      this.sendState()

      clearTimeout(player.timeout)
      player.timeout = setTimeout(() => this.resetToIdlePosition(player, playerIndex), 1000)
    } else {
      this.state.world[0][playerIndex] = '0'
      this.state.world[1][playerIndex] = '0'

      this.sendState()
    }
  }

  resetToIdlePosition(player, playerIndex) {
    clearTimeout(player.timeout)
    if (this.getPlayerColumn(playerIndex)[1] !== player.playerId) {
      if (['1', '2'].find(obstacleType => this.getPlayerColumn(playerIndex)[0] === obstacleType || this.getPlayerColumn(playerIndex)[2] === obstacleType)) {
        this.state.world[1][playerIndex] = '0'
      } else {
        this.state.world[0][playerIndex] = '0'
        this.state.world[1][playerIndex] = player.playerId
        this.state.world[2][playerIndex] = '0'
      }

      this.sendState()
    }
  }

  updateState() {
    let dateInSeconds = Date.now()
    if (dateInSeconds - this.startDate > this.config.timeBeforeStarting) {
      let timeSinceLastUpdate = this.lastUpdateDate ? dateInSeconds - this.lastUpdateDate : null
      if (!timeSinceLastUpdate || timeSinceLastUpdate >= this.config.timeBetweenTicks / this.state.speed) {
        this.lastUpdateDate = dateInSeconds

        // UPGRADE SPEED OVER TIME
        this.state.speed = this.config.speed + (dateInSeconds - this.startDate) / this.config.timeToIncreaseSpeed

        // UPDATE DESTROYED OBJECTS COUNTER
        const firstColumn = [
          this.state.world[0][0],
          this.state.world[1][0],
          this.state.world[2][0]
        ]
        if (firstColumn.find(tile => tile === '1' || tile === '2' || tile === '3')) {
          this.state.destroyedObjects++
        }

        // MOVE ROCKS AND BIRDS RIGHT TO LEFT
        for (let columnIndex = 0; columnIndex < this.config.columns - 1; columnIndex++) {
          for (let rowIndex = 0; rowIndex < this.config.rows; rowIndex++) {
            if (this.state.world[rowIndex][columnIndex].length === 1 && this.state.world[rowIndex][columnIndex + 1].length === 1) {
              this.state.world[rowIndex][columnIndex] = this.state.world[rowIndex][columnIndex + 1]
            } else {
              const playerInScoreboard = this.state.scoreboard.find(playerItem => playerItem.playerId === this.state.world[rowIndex][columnIndex])
              if (playerInScoreboard) {
                playerInScoreboard.points = (dateInSeconds - this.startDate) * this.state.speed
              }
              if (rowIndex === 1) {
                if (['1', '2'].find(obstacleType => this.state.world[2][columnIndex + 1] === obstacleType || this.state.world[0][columnIndex + 1] === obstacleType)) {
                  this.state.world[rowIndex][columnIndex] = '0'
                }
              } else {
                if (['1', '2'].find(obstacleType => this.state.world[rowIndex][columnIndex + 1] === obstacleType)) {
                  this.state.world[rowIndex][columnIndex] = '0'
                } else if (this.state.world[rowIndex][columnIndex + 1] === '3') {
                  this.state.world[rowIndex][columnIndex + 1] = '0'
                  this.state.destroyedObjects++
                  this.creditOneBoxToAllPlayers()
                }
              }
            }
          }
        }

        this.state.world[0][this.config.columns - 1] = '0'
        this.state.world[1][this.config.columns - 1] = '0'
        this.state.world[2][this.config.columns - 1] = '0'


        // SPAWN NEW OBSTACLES
        let threeLastColumns = [
          this.state.world[this.config.rows - 1][this.config.columns - 2],
          this.state.world[this.config.rows - 2][this.config.columns - 2],
          this.state.world[this.config.rows - 3][this.config.columns - 2],
          this.state.world[this.config.rows - 1][this.config.columns - 3],
          this.state.world[this.config.rows - 2][this.config.columns - 3],
          this.state.world[this.config.rows - 3][this.config.columns - 3],
          this.state.world[this.config.rows - 1][this.config.columns - 4],
          this.state.world[this.config.rows - 2][this.config.columns - 4],
          this.state.world[this.config.rows - 3][this.config.columns - 4],
        ]

        if (threeLastColumns.find(tile => tile !== '0') === undefined) {
          const type = Math.round(Math.random() * 5) === 0 ? '3' : '' + (1 + Math.round(Math.random()))
          const place = Math.round(Math.random()) === 0 ? 0 : 2
          this.state.world[place][this.config.columns - 1] = type
          this.state.spawnedObjects++
        }

        this.sendState()
      }
    }
  }

  async creditOneBoxToAllPlayers() {
    this.members.filter(member => member.loggedIn).forEach(async member => {
      let user = await MongoUtils.getUserWithId(member.id)
      const updatedUser = await MongoUtils.updateUser(user.id, { boxes: user.boxes + 1 })
      updatedUser.unlockedAvatars = await AvatarController.avatars(user)
      updatedUser.avatarImage = user.unlockedAvatars.find(x => x.id === updatedUser.avatar.id).image
      UserController.userUpdated(updatedUser)
    })
  }

  sendState() {
    this.screenSocket.broadcast(
      (client, socket) => !client.attr().isController,
      JSON.stringify({ title: 'minigame-update', data: { state: this.state } })
    )
  }

  stop() {
    clearInterval(this.clock)
    clearTimeout(this.timeout)
    this.clock = null
  }

}

export default Runner