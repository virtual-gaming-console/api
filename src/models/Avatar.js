class Avatar {
  _id = null
  names = { fr: null, en: null }
  image = null
  shinyImage = null
  rank = null
  childIndex = null
  index = null
  isLegendary = null

  constructor(fr, en, image, shinyImage, rank, index, childIndex, isLegendary) {
    this.names = { fr, en }
    this.image = image
    this.shinyImage = shinyImage
    this.rank = rank
    this.index = index
    this.childIndex = childIndex
    this.isLegendary = isLegendary || false
  }
}

export default Avatar