class Lobby {
  id = null
  code = null
  members = null
  activeMinigame = null
  socket = null

  constructor(id, socket) {
    this.id = id
    this.code = ('' + id).split('').reverse().splice(0, 6).join('')
    this.members = []
    this.socket = socket
  }
}

export default Lobby