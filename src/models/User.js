class User {
  id = null
  email = null
  nickname = null
  avatar = {
    id: null,
    shiny: false
  }
  coins = 2500
  unlockedAvatars = []
  ownedAvatars = []
  boxes = 0

  constructor(id, email) {
    this.id = id
    this.email = email
  }
}

export default User