import Lobby from "../models/Lobby"
import LobbyMember from "../models/LobbyMember"
import MongoUtils from '../config/mongo-utils'
import AvatarController from './AvatarController'
import { Router } from 'express'
import MINIGAMES from '../mocks/MINIGAMES'

class LobbyController {
  static lobbys = []

  static router = () => {
    const router = Router()

    router.get('/check/:code', (req, res) => {
      const { code } = req.params
      res.json({ exist: LobbyController.lobbys.find(lobby => lobby.code === code) !== undefined })
    })

    router.get('/:code', (req, res) => {
      const { code } = req.params
      let lobby = LobbyController.lobbys.find(lobby => lobby.code === code)
      //ADD ERROR HANDLING
      res.json(lobby)
    })

    router.get('/:code/:appUrl/start-controllers', (req, res) => {
      const { code, appUrl } = req.params
      let lobby = LobbyController.lobbys.find(lobby => lobby.code === code)
      if (lobby) {
        lobby.members.forEach(member => {
          member.socket.send(JSON.stringify({ title: 'screen-action', data: { action: 'open-controller', content: appUrl + '://ontestdesbails' } }))
        });
      }

      res.json({ ...lobby, members: lobby.members.map(({ socket, ...member }) => member) })
    })

    return router
  }

  static onMessage = (socket, message) => {
    switch (message.title) {
      case 'lobby-create':
        LobbyController.createLobby(message.data, socket)
        break
      case 'lobby-connect':
        LobbyController.connectLobby(message.data, socket)
        break
      case 'lobby-disconnect':
        LobbyController.disconnectLobby(message.data, socket)
        break
      case 'lobby-start-minigame':
        LobbyController.startMinigame(message.data, socket)
        break
      case 'lobby-stop-minigame':
        LobbyController.stopMinigame(message.data, socket)
        break
      case 'lobby-update-member':
        LobbyController.updateMember(message.data, socket)
        break
      case 'controller-action':
        LobbyController.controllerAction(message.data, socket)
        break
      case 'screen-action':
        LobbyController.screenAction(message.data, socket)
        break
      case 'lobby-destroy':
        LobbyController.destroyLobby(message.data, socket)
        break
    }
  }

  static createLobby = (data, socket) => {
    const lobby = new Lobby(data.id, socket)

    LobbyController.lobbys = [
      ...LobbyController.lobbys,
      lobby
    ]

    socket.join(lobby.code)
    socket.attr().isController = false

    socket.send(JSON.stringify({ title: 'lobby-validate', data: { code: lobby.code } }))
  }

  static controllerAction = (data, socket) => {
    const code = socket.rooms[0]
    const lobby = LobbyController.lobbys.find(x => x.code === code)
    const member = lobby.members.find(x => x.id == data.memberId).get()
    socket.broadcastToOthers(
      (client, socket) => !client.attr().isController,
      JSON.stringify({ title: 'controller-action', data: { action: data.action, member, content: data.content } })
    )
    if (lobby.activeMinigame && !lobby.activeMinigame.stopped) {
      lobby.activeMinigame.processInput('' + member.id, data.action)
    }
  }

  static screenAction = (data, socket) => {
    const code = socket.rooms[0]
    const lobby = LobbyController.lobbys.find(x => x.code === code)
    const member = lobby.members.find(x => x.id == data.memberId).get()
    socket.broadcast(
      (client, socket) => client.attr().memberId == data.memberId,
      JSON.stringify({ title: 'screen-action', data: { action: data.action, member, content: data.content } })
    )
  }

  static connectLobby = async (data, socket) => {
    const lobby = LobbyController.lobbys.find(x => x.code === data.code)
    let lobbyMember
    if (data.id) {
      let user = await MongoUtils.getUserWithId(data.id)
      user.unlockedAvatars = await AvatarController.avatars(user)
      user.avatarImage = user.unlockedAvatars.find(x => x.id === user.avatar.id).image
      lobbyMember = new LobbyMember(user.id, user.nickname, user.avatarImage, lobby.members.length === 0)
    } else {
      lobbyMember = new LobbyMember(null, data.nickname || null, null, lobby.members.length === 0)
    }

    lobbyMember.socket = socket

    lobby.members = [
      ...lobby.members,
      lobbyMember
    ]

    socket.join(lobby.code)
    socket.attr().isController = true
    socket.attr().memberId = lobbyMember.id
    socket.broadcast(
      (client, socket) => !client.attr().isController,
      JSON.stringify({ title: 'lobby-controller-connected', data: { member: lobbyMember.get() } })
    )
    socket.send(JSON.stringify({ title: 'lobby-connect-success', data: { member: lobbyMember.get() } }))

    if (!lobby.activeMinigame) {
      lobby.activeMinigame = MINIGAMES[Math.floor(Math.random() * MINIGAMES.length)](lobby.socket)
    }
    lobby.activeMinigame.members = lobby.members
  }

  static startMinigame = (data, socket) => {
    const lobby = LobbyController.lobbys.find(x => x.code === data.code)
    if (lobby) {
      lobby.activeMinigame.start()
    }
  }

  static stopMinigame = (data, socket) => {
    const lobby = LobbyController.lobbys.find(x => x.code === data.code)
    if (lobby) {
      lobby.activeMinigame.stop()
    }
  }

  static updateMember = (data, socket) => {
    const lobby = LobbyController.lobbys.find(x => x.code === data.code)
    const lobbyMember = lobby.members.find(member => member.id === data.id)

    if (data.nickname) {
      lobbyMember.nickname = data.nickname
    }
    if (data.picture) {
      lobbyMember.picture = data.picture
    }
    if (data.isLobbyLeader) {
      lobbyMember.isLobbyLeader = data.isLobbyLeader
    }

    socket.broadcast(
      (client, socket) => !client.attr().isController,
      JSON.stringify({ title: 'lobby-member-updated', data: { member: lobbyMember.get() } })
    )
    socket.send(JSON.stringify({ title: 'lobby-member-updated', data: { member: lobbyMember.get() } }))
  }

  static disconnectLobby = (data, socket) => {
    const lobby = LobbyController.lobbys.find(x => x.code === data.code)
    const lobbyMember = { ...(lobby.members.find(member => member.id == data.member.id)) }

    lobby.members = [
      ...lobby.members.filter(x => x.id != data.member.id)
    ]

    socket.broadcast(
      (client, socket) => !client.attr().isController,
      JSON.stringify({ title: 'lobby-controller-disconnected', data: { id: lobbyMember.id } })
    )

    if (lobbyMember.isLobbyLeader && lobby.members.length > 0) {
      LobbyController.updateMember({ code: data.code, id: lobby.members[0].id, isLobbyLeader: true }, socket)
    }

    socket.leave(lobby.code)

    if (lobby.activeMinigame && !lobby.members.length) {
      lobby.activeMinigame.stop()
    } else {
      lobby.activeMinigame.members = lobby.members
    }
  }

  static destroyLobby = (data, socket) => {
    console.log('---- LOBBY DESTROY ----')
    console.log(data)
  }
}

export default LobbyController