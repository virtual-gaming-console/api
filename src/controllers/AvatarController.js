import { Router } from 'express'
import AVATARS from '../mocks/AVATARS'
import MongoUtils from '../config/mongo-utils'
import Avatar from '../models/Avatar'
import Config from '../config'

const URL = Config.protocol === "https" ? `${Config.protocol}://${Config.host}` : `${Config.protocol}://${Config.host}:${Config.port}`

class AvatarController {
  static cost = 500

  static inventory = async (user) => {

    const inventory = {
      ownedAvatars: user.ownedAvatars || []
    }

    if (inventory.ownedAvatars.length === 0) {
      return inventory
    }

    const avatars = await MongoUtils.getAvatarsWithParams()

    for (var i = 0; i < inventory.ownedAvatars.length; i++) {
      const { _id, ...avatar } = avatars.find(avatarItem => (avatarItem._id + '') == inventory.ownedAvatars[i].avatarId)
      delete inventory.ownedAvatars[i].avatarId
      inventory.ownedAvatars[i].avatar = { id: _id, ...avatar }
      const evolution = avatars.find(avatarItem => avatarItem.childIndex === avatar.index)
      inventory.ownedAvatars[i].avatar.evolution = evolution ? 'y' + evolution.image : null
      if (inventory.ownedAvatars[i].avatar.isLegendary) {
        inventory.ownedAvatars[i].cost = 2000
      } else if (inventory.ownedAvatars[i].avatar.rank === 1) {
        inventory.ownedAvatars[i].cost = 100
      } else if (inventory.ownedAvatars[i].avatar.rank === 2) {
        inventory.ownedAvatars[i].cost = 210
      } else if (inventory.ownedAvatars[i].avatar.rank === 3) {
        inventory.ownedAvatars[i].cost = 500
      }
    }

    return inventory
  }

  static avatars = async (user) => {
    const avatars = [...user.unlockedAvatars]
    const avatarModels = await MongoUtils.getAvatarsWithParams()

    for (let index = 0; index < avatars.length; index++) {
      const avatarItem = avatars[index];
      const avatarModel = avatarModels.find(model => (model._id + '') == avatarItem.id)
      avatarItem.image = `${URL}/assets/images/avatars/${avatarItem.shiny ? avatarModel.shinyImage : avatarModel.image}`
      avatarItem.rank = avatarModel.rank
      avatarItem.isLegendary = avatarModel.isLegendary
      avatarItem.index = avatarModel.index
    }

    return avatars
  }

  static router = () => {
    const router = Router()

    // CRUD OPERATIONS TO CREATE HERE

    router.get('/:avatarId', async function (req, res) {
      const avatarId = req.params.avatarId
      const user = req.user

      if (!user) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'You are not logged in or session expired.'
        })
      }

      if (!avatarId) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Request broken. YOU BITCH.'
        })
      }

      const avatarModel = await MongoUtils.getAvatarWithId(avatarId)

      if (!avatarModel) {

        return res.status(400).json({
          success: false,
          code: 2,
          message: 'This avatar no longer exists. APOLOGIES.'
        })
      } else {

        return res.json({
          success: true,
          data: avatarModel
        })
      }

    })

    router.delete('/:avatarId', async function (req, res) {
      const avatarId = req.params.avatarId
      const user = req.user

      if (!user) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'You are not logged in or session expired.'
        })
      }

      if (!avatarId) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Request broken. YOU BITCH.'
        })
      }

      // Vérification existance en BD
      const avatarModel = await MongoUtils.getAvatarWithId(avatarId)

      if (!avatarModel) {

        return res.status(400).json({
          success: false,
          code: 2,
          message: 'This avatar no longer exists. APOLOGIES.'
        })
      } else {

        const result = await MongoUtils.deleteAvatar(avatarId)

        if (result) {
          return res.json({
            success: true,
            data: avatarModel
          })
        }

        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Error in DB while trying to delete an avatar.'
        })
      }

    })

    router.post('/', async function (req, res) {
      const user = req.user
      const avatarToAdd = req.body

      if (!user) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'You are not logged in or session expired.'
        })
      }

      if (!avatarToAdd) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Request broken. YOU BITCH.'
        })
      }

      if (!avatarToAdd.names || !avatarToAdd.names.en) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Names or EN_Name is missing.'
        })
      }

      if (!avatarToAdd.index) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Index is missing.'
        })
      }

      // Recherches en base
      const avatarModel = await MongoUtils.getAvatarWithParams({ index: avatarToAdd.index })

      if (!avatarModel) { // N'existe pas déjà en BD

        let newAvatar = new Avatar(
          avatarToAdd.names && avatarToAdd.names.fr || 'avatar_name_fr',
          avatarToAdd.names && avatarToAdd.names.en || 'avatar_name_en',
          avatarToAdd.image || '',
          avatarToAdd.shinyImage || '',
          avatarToAdd.rank || 1,
          avatarToAdd.index,
          avatarToAdd.childIndex || null,
          avatarToAdd.isLegendary
        )

        const avatarAdded = await MongoUtils.insertAvatar(newAvatar)

        return res.json({
          success: true,
          data: avatarAdded
        })

      } else {

        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Avatar with this index already exists.'
        })
      }

    })

    router.patch('/', async function (req, res) {
      const user = req.user
      const avatarToUpdate = req.body

      if (!user) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'You are not logged in or session expired.'
        })
      }

      if (!avatarToUpdate) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Request broken. YOU BITCH.'
        })
      }

      if (!avatarToUpdate.names || !avatarToUpdate.names.en) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Names or EN_Name is missing.'
        })
      }

      if (!avatarToUpdate.index) {
        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Index is missing.'
        })
      }

      // Recherches en base
      const avatarModel = await MongoUtils.getAvatarWithParams({ index: avatarToUpdate.index })

      if (!avatarModel) { // N'existe pas déjà en BD

        return res.status(400).json({
          success: false,
          code: 2,
          message: 'Avatar with this index already exists.'
        })

      } else { // Il existe il faut le modifier

        let newAvatar = new Avatar(
          avatarToUpdate.names && avatarToUpdate.names.fr || avatarModel.names.fr,
          avatarToUpdate.names && avatarToUpdate.names.en || avatarModel.names.en,
          avatarToUpdate.image || avatarModel.names.image,
          avatarToUpdate.shinyImage || avatarModel.names.shinyImage,
          avatarToUpdate.rank || avatarModel.names.rank,
          avatarToUpdate.index,
          avatarToUpdate.childIndex || avatarModel.names.childIndex,
          avatarToUpdate.isLegendary || avatarModel.names.isLegendary
        )

        const avatarUpdated = await MongoUtils.updateAvatar(avatarModel._id, newAvatar)

        return res.json({
          success: true,
          data: avatarUpdated
        })

      }

    })

    return router
  }

  static initDb = async (callback) => {
    // INIT AVATAR TABLE FROM AVATAR LIST
    console.log('AvatarController.initDb()')

    const firstAvatarExist = await MongoUtils.getAvatarWithParams({ name: AVATARS[0].name })

    if (!firstAvatarExist) {
      await MongoUtils.insertAvatars(AVATARS)
    }

    // execute callback when db initialized
    callback()
  }
}

export default AvatarController