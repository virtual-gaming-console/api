import { Router } from 'express'
import jwt from 'jsonwebtoken'
import GoogleUtils from '../config/google-utils'
import MongoUtils from '../config/mongo-utils'
import Config from '../config'
import User from '../models/User'
import AvatarController from './AvatarController'
import LobbyController from './LobbyController'
import GAMES from '../mocks/GAMES'

const QUEST_STATUS = {
    PENDING: "QUEST_STATUS_PENDING",
    COMPLETED: "QUEST_STATUS_COMPLETED",
    CLAIMED: "QUEST_STATUS_CLAIMED",
}

const dailyLoginQuest = {
    code: 'VGC_DAILY_LOGIN',
    title: 'Daily login',
    text: 'Login everyday to get a free loot box !',
    count: 1,
    reward: {
        boxes: 1
    },
    status: QUEST_STATUS.PENDING
}

class UserController {
    static connectedUsers = []
    static dailyQuests = []

    static router = () => {
        const router = Router()

        /*** 
         * USER'S LOGIN
        ***/
        router.use('/google-auth', async function (req, res) {
            const code = req.query.code
            const user = req.body

            let googleUser = user
            if (code) {
                googleUser = await GoogleUtils.getGoogleAccountFromCode(code)
            }

            if (!googleUser) {
                console.log("CODE: " + code)
                return res.status(400).json({
                    success: false,
                    code: 1,
                    message: 'An error occured while signing you in.'
                })
            }

            let userInDatabase = await MongoUtils.getUserWithEmail(googleUser.email)
            if (!userInDatabase) {
                const userToCreate = new User(googleUser.id, googleUser.email)
                const avatarsRank1 = await MongoUtils.getAvatarsWithParams({ rank: 1, isLegendary: false })
                const randomAvatar = avatarsRank1[Math.floor(Math.random() * avatarsRank1.length)]

                userToCreate.avatar = {
                    id: (randomAvatar._id + ''),
                    shiny: (Math.floor(Math.random() * 100) === 5),
                    date: Date.now()
                }

                userToCreate.unlockedAvatars = [userToCreate.avatar]

                userInDatabase = await MongoUtils.insertUser(userToCreate)
            }

            if (!userInDatabase) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'An error occured while signing you in.'
                })
            }

            let token = jwt.sign({ id: userInDatabase.id },
                Config.secret,
                {
                    expiresIn: '24h'
                }
            );

            if (code) {
                res.redirect(Config.clientUrl + '/auth?token=' + token)
            } else {
                res.json({
                    success: true,
                    token
                })
            }
        })

        router.get('/me', async function (req, res) {
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            user.unlockedAvatars = await AvatarController.avatars(user)
            user.avatarImage = user.unlockedAvatars.find(x => x.id === user.avatar.id).image

            const date = new Date()
            const day = date.getDate() + (date.getMonth() - 1) * 30 + (date.getFullYear() - 1) * 365
            if (!user.dailys || !user.dailys.resetTime || user.dailys.date !== day) {
                let secondsUntilEndOfDate = (24 * 60 * 60) - (date.getHours() * 60 * 60) - (date.getMinutes() * 60) - date.getSeconds();
                user.dailys = {
                    resetTime: secondsUntilEndOfDate,
                    date: day,
                    quests: UserController.dailyQuests.map(quest => ({ ...quest, count: 0 }))
                }

                const dailyLogin = user.dailys.quests.find(quest => quest.code === 'VGC_DAILY_LOGIN')
                if (dailyLogin) {
                    dailyLogin.status = QUEST_STATUS.COMPLETED
                    dailyLogin.count = 1
                }

                await MongoUtils.updateUser(user.id, { dailys: user.dailys })
            }

            let secondsUntilEndOfDate = (24 * 60 * 60) - (date.getHours() * 60 * 60) - (date.getMinutes() * 60) - date.getSeconds();
            user.dailys.resetTime = secondsUntilEndOfDate

            res.json({
                success: true,
                data: user
            })
        })

        router.get('/signin', function (req, res) {
            res.redirect(GoogleUtils.urlGoogle())
        })

        /*** 
         * USER'S PROFILE
        ***/

        router.post('/profile', async function (req, res) {
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            const userUpdateRequest = req.body
            let userUpdate = {}

            if (userUpdateRequest.nickname) {
                if (userUpdateRequest.nickname.length <= 3) {
                    return res.status(400).json({
                        success: false,
                        code: 4,
                        message: 'Nickname too short (under three characters).'
                    })
                } else {
                    userUpdate.nickname = userUpdateRequest.nickname
                }
            }

            if (userUpdateRequest.avatar) {

                if (!userUpdateRequest.avatar.id) {
                    return res.status(400).json({
                        success: false,
                        code: 5,
                        message: `Wrong id.`
                    })
                }

                if (userUpdateRequest.avatar.shiny === null || userUpdateRequest.avatar.shiny === undefined) {
                    return res.status(400).json({
                        success: false,
                        code: 5,
                        message: `Avatar shiny undefined.`
                    })
                }

                const unlockedAvatar = user.unlockedAvatars.find(avatarItem => avatarItem.id === userUpdateRequest.avatar.id && avatarItem.shiny === userUpdateRequest.avatar.shiny)

                if (!unlockedAvatar) {
                    return res.status(400).json({
                        success: false,
                        code: 5,
                        message: `You don't own this avatar MOZERFUCKER.`
                    })
                }

                userUpdate.avatar = unlockedAvatar
            }

            const updatedUser = await MongoUtils.updateUser(user.id, userUpdate)

            updatedUser.unlockedAvatars = await AvatarController.avatars(user)
            updatedUser.avatarImage = user.unlockedAvatars.find(x => x.id === updatedUser.avatar.id).image

            let userInLobby = null
            for (let index = 0; index < LobbyController.lobbys.length; index++) {
                const lobby = LobbyController.lobbys[index];
                userInLobby = lobby.members.find(x => '' + x.id === '' + updatedUser.id)
                if (userInLobby) {
                    LobbyController.updateMember({ id: userInLobby.id, code: lobby.code, picture: updatedUser.avatarImage }, userInLobby.socket)
                    break
                }
            }

            UserController.userUpdated(updatedUser)

            res.json({
                success: true,
                data: updatedUser
            })
        })

        router.get('/avatars', async function (req, res) {
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            const avatars = await AvatarController.avatars(user)

            res.json({
                success: true,
                data: avatars
            })
        })

        /***
         * USER'S QUEST
        ***/

        router.post('/quest/:questCode', async function (req, res) {
            const questCode = req.params.questCode
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            const quest = user.dailys.quests.find(quest => quest.code === questCode)
            if (!quest) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You can\'t claim rewards for a quest you don\'t own.'
                })
            }

            if (quest.status !== QUEST_STATUS.COMPLETED) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You can\'t claim rewards for a quest you didn\'t complete first.'
                })
            }

            quest.status = QUEST_STATUS.CLAIMED
            if (quest.reward && quest.reward.boxes) {
                user.boxes += quest.reward.boxes
            } else if (quest.reward && quest.reward.coins) {
                user.coins += quest.reward.coins
            }

            const updatedUser = await MongoUtils.updateUser(user.id, { coins: user.coins, boxes: user.boxes, dailys: user.dailys })

            res.json({
                success: true,
                data: updatedUser
            })
        })

        /*** 
         * USER'S INVENTORY
        ***/

        router.get('/inventory', async function (req, res) {
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            const inventory = await AvatarController.inventory(user)

            res.json({
                success: true,
                data: inventory
            })
        })

        router.post('/inventory/buy/:qty', async (req, res) => {
            const qty = parseInt((req.params.qty || '0'))
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }
            const totalCost = qty * AvatarController.cost

            if (user.coins < totalCost) {
                return res.status(400).json({
                    success: false,
                    code: 3,
                    message: 'You don\'t have enough coins. GO GET SOME MONEY!'
                })
            }

            const updatedUser = await MongoUtils.updateUser(user.id, { coins: user.coins - totalCost, boxes: (user.boxes || 0) + qty })

            res.json({
                success: true,
                data: updatedUser
            })
        })

        router.post('/inventory/open/:qty', async (req, res) => {
            const qty = parseInt((req.params.qty || '0'))
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            if (user.boxes < qty) {
                return res.status(400).json({
                    success: false,
                    code: 3,
                    message: 'You don\'t have enough boxes. FCKING hacker'
                })
            }

            /********* LOOTS CALCULATION *********/
            let loots = []
            const allAvatars = await MongoUtils.getAvatarsWithParams() // Pull all avatars from DB
            const avatarsRank1 = allAvatars.filter((avatar) => avatar.rank === 1) // Filter rank 1 & legendary
            const legendaryAvatars = avatarsRank1.filter((avatar) => avatar.isLegendary) // Filter only legendary
            const boringAvatars = avatarsRank1.filter((avatar) => !avatar.isLegendary) // Only rank 1 without legendary
            // Rank 1 that cannot evolve
            const rareAvatars = boringAvatars
                .filter((avatar) => allAvatars.find(avatarItem => avatarItem.childIndex === avatar.index) === undefined)
            // Intermediate tab with avatar with at least one evolve
            const rank1WithParent = boringAvatars
                .filter((avatar) => allAvatars.find(avatarItem => avatarItem.childIndex === avatar.index) !== undefined)
                .map(avatarItem => ({ ...avatarItem, parent: allAvatars.find(item => item.childIndex === avatarItem.index) }))
            // Rank 1 that have max rank 3 evolve
            const commonAvatars = rank1WithParent.filter((avatar) => allAvatars.find(avatarItem => avatarItem.childIndex === avatar.parent.index) !== undefined)
            // Rank 1 that have max rank 2 evolve
            const uncommonAvatars = rank1WithParent.filter((avatar) => commonAvatars.find(x => x.index === avatar.index) === undefined)

            for (let i = 0; i < qty; i++) {
                const rng = Math.floor(Math.random() * 100)
                let avatarList = []

                if (rng === 1) { // Legendaire 1/100
                    avatarList = legendaryAvatars
                } else if (rng <= 61) { // Common 60/100
                    avatarList = commonAvatars
                } else if (rng <= 91) { // Uncommon 30/100
                    avatarList = uncommonAvatars
                } else { // Rare 9/100
                    avatarList = rareAvatars
                }

                loots.push({
                    avatarId: '' + avatarList[Math.floor(Math.random() * avatarList.length)]._id,
                    isShiny: (Math.floor(Math.random() * 100) === 5)
                })
            }

            user.ownedAvatars = user.ownedAvatars || []

            for (let i = 0; i < loots.length; i++) {
                const loot = loots[i]

                const ownedAvatar = user.ownedAvatars.find(ownedAvatar => ownedAvatar.avatarId == loot.avatarId)
                if (ownedAvatar) {
                    if (loot.isShiny) {
                        ownedAvatar.shinyQuantity += 1
                    } else {
                        ownedAvatar.quantity += 1
                    }
                } else {
                    user.ownedAvatars.push({
                        avatarId: loot.avatarId,
                        quantity: loot.isShiny ? 0 : 1,
                        shinyQuantity: loot.isShiny ? 1 : 0
                    })
                }

                loot.avatar = avatarsRank1.find(avatar => (avatar._id + '') == loot.avatarId)
                delete loot.avatarId
            }

            const updatedUser = await MongoUtils.updateUser(user.id, { ownedAvatars: user.ownedAvatars, boxes: user.boxes - qty })

            const inventory = await AvatarController.inventory(updatedUser)

            res.json({
                success: true,
                data: {
                    user: updatedUser,
                    loots,
                    inventory
                }
            })
        })

        router.post('/inventory/sell/:avatarId', async (req, res) => {
            const { avatarId } = req.params
            const { shinyQuantity, quantity } = req.body
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            if (!avatarId) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'No avatarId specified.'
                })
            }

            if (!shinyQuantity === undefined) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'No shinyQuantity specified.'
                })
            }

            if (!quantity === undefined) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'No quantity specified.'
                })
            }

            const avatar = user.ownedAvatars.find(avatar => avatar.avatarId === avatarId)

            /******** SELL PRICE CALCULATION ********/
            if (avatar) {
                const avatarModel = await MongoUtils.getAvatarWithId(avatarId) // Pull the avatar to sell from DB
                let avatarCost = 0
                let totalCost = 0

                if (avatarModel.isLegendary) {
                    avatarCost = 2000
                } else if (avatarModel.rank === 1) {
                    avatarCost = 100
                } else if (avatarModel.rank === 2) {
                    avatarCost = 210
                } else if (avatarModel.rank === 3) {
                    avatarCost = 500
                }

                if (avatar.quantity >= quantity) {
                    avatar.quantity -= quantity
                    totalCost += quantity * avatarCost
                } else {
                    return res.status(400).json({
                        success: false,
                        code: 3,
                        message: 'You don\'t have enough of this avatar. SON OF A BITCH!'
                    })
                }

                if (avatar.shinyQuantity >= shinyQuantity) {
                    avatar.shinyQuantity -= shinyQuantity
                    totalCost += shinyQuantity * avatarCost
                } else {
                    return res.status(400).json({
                        success: false,
                        code: 3,
                        message: 'You don\'t have enough shiny version of this avatar. SON OF A BITCH!'
                    })
                }

                user.ownedAvatars = user.ownedAvatars.filter(avatarItem => avatarItem.quantity > 0 || avatarItem.shinyQuantity > 0) // On garde que quand yen a plus de 0

                const updatedUser = await MongoUtils.updateUser(user.id, {
                    coins: user.coins + totalCost,
                    ownedAvatars: user.ownedAvatars
                })

                const inventory = await AvatarController.inventory(updatedUser)

                return res.json({
                    success: true,
                    data: {
                        inventory,
                        user: updatedUser,
                        money: totalCost
                    }
                })

            } else {
                return res.status(400).json({
                    success: false,
                    code: 3,
                    message: 'You don\'t have this avatar. SON OF A BITCH!'
                })
            }
        })

        router.post('/inventory/fuse/:avatarId', async (req, res) => {
            const avatarId = req.params.avatarId
            const { shinyQuantity, quantity } = req.body
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            if (!avatarId) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'Request broken. YOU BITCH.'
                })
            }

            if (shinyQuantity === undefined) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'No shinyQuantity specified.'
                })
            }

            if (quantity === undefined) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'No quantity specified.'
                })
            }

            const avatar = user.ownedAvatars.find(avatarItem => avatarItem.avatarId === avatarId)

            if (avatar) {
                const avatarModel = await MongoUtils.getAvatarWithId(avatarId)

                if (!avatarModel) {
                    return res.status(400).json({
                        success: false,
                        code: 3,
                        message: 'This avatar no longer exist, SORRY BRO!'
                    })
                }

                const parentsModel = await MongoUtils.getAvatarsWithParams({ childIndex: avatarModel.index }) // Can return several

                if (parentsModel) {
                    // If multiple parent, keep randomly one
                    const parent = parentsModel[Math.floor(Math.random() * parentsModel.length)]

                    const fusedAvatar = {
                        avatarId: (parent._id + ''),
                        shinyQuantity: 0,
                        quantity: 0
                    }

                    /********** Shiny calculation ***********/
                    let isShiny = false

                    if (shinyQuantity === 0 && quantity === 2) { // If 2 regular avatars are fused
                        avatar.quantity -= 2
                        fusedAvatar.quantity++
                    } else if (shinyQuantity === 2 && quantity === 0) { // If 2 shiny avatars are fused
                        avatar.shinyQuantity -= 2
                        fusedAvatar.shinyQuantity++
                        isShiny = true // The result is 100% chance shiny
                    } else if (shinyQuantity === 1 && quantity === 1) { // If 1 regular & 1 shiny are fused
                        avatar.shinyQuantity--
                        avatar.quantity--
                        if (Math.random() >= .5) { // Only 50% chance to get a shiny parent
                            fusedAvatar.shinyQuantity++
                            isShiny = true
                        } else {
                            fusedAvatar.quantity++
                        }
                    } else {
                        return res.status(400).json({
                            success: false,
                            code: 3,
                            message: 'Wrong avatar quantity for fusion !'
                        })
                    }

                    if (avatar.quantity < 0 || avatar.shinyQuantity < 0) {
                        return res.status(400).json({
                            success: false,
                            code: 3,
                            message: 'Wrong avatar quantity for fusion ! LITTLE HACKER'
                        })
                    }

                    user.ownedAvatars = user.ownedAvatars.filter(avatarItem => avatarItem.quantity > 0 || avatarItem.shinyQuantity > 0) // Filter to remove entries where avatar count = 0

                    const parentAvatar = user.ownedAvatars.find(avatarItem => avatarItem.avatarId == (parent._id + ''))

                    if (parentAvatar) {
                        parentAvatar.quantity += fusedAvatar.quantity
                        parentAvatar.shinyQuantity += fusedAvatar.shinyQuantity
                    } else {
                        user.ownedAvatars.push(fusedAvatar)
                    }

                    const updatedUser = await MongoUtils.updateUser(user.id, { ownedAvatars: user.ownedAvatars })
                    const inventory = await AvatarController.inventory(updatedUser)

                    res.json({
                        success: true,
                        data: {
                            inventory,
                            fusedAvatar,
                            isShiny
                        }
                    })
                } else {
                    return res.status(400).json({
                        success: false,
                        code: 3,
                        message: 'You don\'t have enough of this avatar. FCKING RETARD!'
                    })
                }

            } else {
                return res.status(400).json({
                    success: false,
                    code: 3,
                    message: 'You don\'t have this avatar. FCKING RETARD!'
                })
            }


        })

        router.post('/inventory/unlock/:avatarId', async (req, res) => {
            const avatarId = req.params.avatarId
            const isShiny = req.body.isShiny
            const user = req.user

            if (!user) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'You are not logged in or session expired.'
                })
            }

            if (!avatarId) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'Request broken. YOU BITCH.'
                })
            }

            if (isShiny === null || isShiny === undefined) {
                return res.status(400).json({
                    success: false,
                    code: 2,
                    message: 'No isShiny specified.'
                })
            }

            const avatar = user.ownedAvatars.find(avatarItem => avatarItem.avatarId === avatarId)

            if (avatar) {
                if (isShiny && avatar.shinyQuantity < 1) {
                    return res.status(400).json({
                        success: false,
                        code: 3,
                        message: 'You don\'t have enough shiny version of this avatar. FCKING RETARD!'
                    })
                }

                if (!isShiny && avatar.quantity < 1) {
                    return res.status(400).json({
                        success: false,
                        code: 3,
                        message: 'You don\'t have enough normal version of this avatar. FCKING RETARD!'
                    })
                }

                if (user.unlockedAvatars.find(unlockedAvatarItem => unlockedAvatarItem.id === avatarId && unlockedAvatarItem.shiny === isShiny)) {
                    return res.status(400).json({
                        success: false,
                        code: 3,
                        message: 'You can\'t unlock an avatar you already own.'
                    })
                }

                if (isShiny) {
                    avatar.shinyQuantity--
                } else {
                    avatar.quantity--
                }

                let unlockedAvatar = {
                    id: avatarId,
                    shiny: isShiny,
                    date: Date.now()
                }

                user.ownedAvatars = user.ownedAvatars.filter(avatarItem => avatarItem.quantity > 0 || avatarItem.shinyQuantity > 0)
                user.unlockedAvatars.push(unlockedAvatar)

                const updatedUser = await MongoUtils.updateUser(user.id, { ownedAvatars: user.ownedAvatars, unlockedAvatars: user.unlockedAvatars })
                const inventory = await AvatarController.inventory(updatedUser)

                updatedUser.unlockedAvatars = await AvatarController.avatars(user)
                unlockedAvatar = user.unlockedAvatars.find(x => x.id === unlockedAvatar.id)

                res.json({
                    success: true,
                    data: {
                        inventory,
                        user: updatedUser,
                        unlockedAvatar
                    }
                })
            } else {
                return res.status(400).json({
                    success: false,
                    code: 3,
                    message: 'You don\'t have this avatar. FCKING RETARD!'
                })
            }
        })

        return router
    }

    static onMessage = (socket, message) => {
        switch (message.title) {
            case 'user-connected':
                UserController.userConnected(message.data, socket)
                break
        }
    }

    static onDisconnect = socket => {
        UserController.connectedUsers = UserController.connectedUsers.filter(x => x.id !== socket.id)
    }

    static userConnected = (data, socket) => {
        socket.attr().userId = data.id
        UserController.connectedUsers.push(socket)
    }

    static userUpdated = (user) => {
        const sockets = UserController.connectedUsers.filter(x => x.attr().userId == user.id)
        for (let index = 0; index < sockets.length; index++) {
            const socket = sockets[index];
            socket.send(JSON.stringify({ title: 'user-updated', data: { user } }))
        }
    }

    static authMiddleware = (req, res, next) => {
        let token = req.headers['x-access-token'] || req.headers['authorization']
        if (token && token.startsWith('Bearer ')) {
            token = token.slice(7, token.length);
        }

        if (token) {
            jwt.verify(token, Config.secret, async (err, decoded) => {
                if (!err) {
                    let userInDatabase = await MongoUtils.getUserWithId(decoded.id)

                    if (userInDatabase) {
                        req.user = userInDatabase
                    }
                } else {
                    console.log(err)
                }
                next()
            });
        } else {
            next()
        }

    }

    static init = () => {
        UserController.generateQuests()

        let date = new Date();
        let secondsUntilEndOfDate = (24 * 60 * 60) - (date.getHours() * 60 * 60) - (date.getMinutes() * 60) - date.getSeconds();

        setTimeout(() => {
            UserController.generateQuests()
            setInterval(() => {
                UserController.generateQuests()
                // UserController.synchroniseQuests()
            }, 24 * 60 * 60 * 1000)
        }, secondsUntilEndOfDate * 1000)
    }

    static generateQuests = () => {
        UserController.dailyQuests = [dailyLoginQuest]

        let gamesWithQuests = JSON.parse(JSON.stringify(GAMES.filter(game => game.quests !== undefined && game.quests.dailys !== undefined && game.quests.dailys.length > 0)))
        if (!gamesWithQuests.length) {
            return
        }

        for (let index = 0; index < 7; index++) {
            const { quests, ...game } = gamesWithQuests[Math.floor(Math.random() * gamesWithQuests.length)]
            const quest = quests.dailys[Math.floor(Math.random() * quests.dailys.length)]
            UserController.dailyQuests.push({
                ...quest,
                status: QUEST_STATUS.PENDING,
                relatedGame: game
            })
            quests.dailys = quests.dailys.filter(questItem => questItem.code !== quest.code)
            gamesWithQuests = gamesWithQuests.filter(game => game.quests.dailys !== undefined && game.quests.dailys.length > 0)

            if (gamesWithQuests.length === 0) {
                break
            }
        }
    }
}

export default UserController