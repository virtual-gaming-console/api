import { Router } from 'express'
import GAMES from '../mocks/GAMES'
import CONFIG from '../config'

const URL = CONFIG.protocol === "https" ? `${CONFIG.protocol}://${CONFIG.host}` : `${CONFIG.protocol}://${CONFIG.host}:${CONFIG.port}`

class GameController {

    static router = () => {
        const router = Router()

        router.get('/', function (req, res) {
            res.json(GAMES.map(x => ({ ...x, picture: URL + x.picture })))
        })

        router.get('/:gameTag', function (req, res) {
            const { gameTag } = req.params
            const firstChar = gameTag.split().shift()

            let game = null

            if (/[0-9]/g.test(firstChar)) {
                game = GAMES.find(x => x.id == firstChar)
            } else {
                game = GAMES.find(game => gameTag === game.title.replace(" ", "-").toLowerCase())
            }

            if (game) {
                res.json({ ...game, picture: URL + game.picture })
            } else {
                res.status(400).json({
                    error: 'Game not found !'
                })
            }
        })

        return router
    }

    static onMessage = (socket, message) => {
        // NOT IMPLEMTED YET

        // switch (message.title) {
        //     case 'user-connected':
        //         GameController.userConnected(message.data, socket)
        //         break
        // }
    }

    static onDisconnect = socket => {
        // NOT IMPLEMTED YET
    }
}

export default GameController