const GAMES = [
    {
        id: 1,
        title: 'Munchkin',
        category: 'RPG card game',
        description: 'Open the gates and destroy all monsters with your friends !',
        picture: '/assets/images/munchkin.jpg',
        url: 'https://client.munchkin.playdeviant.com/',
        playersMin: 2,
        playersMax: 4,
        status: "ONLINE",
    },
    {
        id: 2,
        title: 'Picolo',
        category: 'Drinking party game',
        description: 'Plays with your friends around fun challenges, loosers get to drink !',
        picture: '/assets/images/super_picolo.png',
        url: 'http://localhost:5000/',
        playersMin: 2,
        playersMax: 14,
        status: "IDEA",
    },
    {
        id: 3,
        title: 'Tower Of Ascension',
        category: 'RPG rogue-like',
        description: 'Choose your party and climbs the floors of the almighty tower !',
        picture: '/assets/images/toa.jpg',
        url: 'http://localhost:5000/',
        playersMin: 1,
        playersMax: 1,
        status: "IDEA",
    }
]

export default GAMES