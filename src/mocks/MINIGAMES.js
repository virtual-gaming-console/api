import Runner from '../models/Runner'

const MINIGAMES = [
  (socket, playerIds, config = null) => new Runner(socket, playerIds, config)
]

export default MINIGAMES